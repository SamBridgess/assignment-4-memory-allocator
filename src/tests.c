#ifndef _TESTS_
#define _TESTS_

#include <stdio.h>
#define __USE_MISC 1
#include "tests_util.h"
#include "tests.h"

//1
int simple_alloc_test() {
    printf("Test 1, simple allocation: \n");

    printf("Trying to initialize heap...    ");
    void* heap = heap_init(HEAP_SIZE);
    printf("Success!\n");
    printf("Trying to initialize block...   ");
    void* b1 = _malloc(BLOCK_SIZE);
    printf("Success!\n");

    if (b1 == NULL) {
        printf("Block corrupted!\n");
        return 0;
    }
    if(heap == NULL) {
        printf("Heap corrupted!\n");
        return 0;
    }
    printf("Block and Heap OK\n");

    _free(b1);
   
    munmap(heap, get_block_size(b1, THIS));
    return 1;
}

//2
int free_one_block_test() {
    printf("Test 2, free one block out of few selected: \n");

    printf("Trying to initialize heap...    ");
    void* heap = heap_init(HEAP_SIZE);
    printf("Success!\n");

    printf("Trying to initialize block 1...   ");
    void* b1 = _malloc(BLOCK_SIZE);
    printf("Success!\n");

    printf("Trying to initialize block 2...   ");
    void* b2 = _malloc(BLOCK_SIZE);
    printf("Success!\n");

    printf("Trying to initialize block 3...   ");
    void* b3 = _malloc(BLOCK_SIZE);
    printf("Success!\n");

    printf("Trying to initialize block 4...   ");
    void* b4 = _malloc(BLOCK_SIZE);
    printf("Success!\n");

    printf("Trying to initialize block 4...   ");
    void* b5 = _malloc(BLOCK_SIZE);
    printf("Success!\n");

    if (heap == NULL){
        printf("Heap corrupted!\n");
        return 0;
    }
     if (b1 == NULL){
        printf("Block 1 corrupted!\n");
        return 0;
    }
     if (b2 == NULL){
        printf("Block 2 corrupted!\n");
        return 0;
    }
    if (b3 == NULL){
        printf("Block 3 corrupted!\n");
        return 0;
    }
    if (b4 == NULL){
        printf("Block 4 corrupted!\n");
        return 0;
    }
    if (b5 == NULL){
        printf("Block 5 corrupted!\n");
        return 0;
    }
    
    
    size_t next_size = get_block_size(b5, NEXT);

    _free(b5);
    printf("Trying to free block 5...   ");
    if (!get_block_is_free(b5)){ 
        printf("Couldn't free block\n");
        return 0;
    }
    printf("Success!\n");

    if(get_block_capacity(b5) != BLOCK_SIZE + next_size) {
        printf("Bad capacity!\n");
        return 0;
    }   
    
    _free(b4);
    _free(b3);
    _free(b2);
    _free(b1);
    munmap(heap, get_block_size(b1, THIS));
    return 1;
}

//3
int free_two_blocks_test() {
    printf("Test 3, free two blocks out of few selected: \n");

    printf("Trying to initialize heap...    ");
    void* heap = heap_init(HEAP_SIZE);
    printf("Success!\n");

    printf("Trying to initialize block 1...   ");
    void* b1 = _malloc(BLOCK_SIZE);
    printf("Success!\n");


    printf("Trying to initialize block 2...   ");
    void* b2 = _malloc(BLOCK_SIZE);
    printf("Success!\n");

    printf("Trying to initialize block 3...   ");
    void* b3 = _malloc(BLOCK_SIZE);
    printf("Success!\n");

    printf("Trying to initialize block 4...   ");
    void* b4 = _malloc(BLOCK_SIZE);
    printf("Success!\n");

    printf("Trying to initialize block 5...   ");
    void* b5 = _malloc(BLOCK_SIZE);
    printf("Success!\n");

    if (heap == NULL){
        printf("Heap corrupted!\n");
        return 0;
    }
     if (b1 == NULL){
        printf("Block 1 corrupted!\n");
        return 0;
    }
     if (b2 == NULL){
        printf("Block 2 corrupted!\n");
        return 0;
    }
    if (b3 == NULL){
        printf("Block 3 corrupted!\n");
        return 0;
    }
    if (b4 == NULL){
        printf("Block 4 corrupted!\n");
        return 0;
    }
    if (b5 == NULL){
        printf("Block 5 corrupted!\n");
        return 0;
    }
   

    size_t b4_size = get_block_size(b4, THIS);
    
    printf("Trying to free block 4...   ");
    _free(b4); 
    if (!get_block_is_free(b4)){ 
        printf("Couldn't free block\n");
        return 0;
    }
    printf("Success!\n");

    printf("Trying to free block 3...   ");
    _free(b3); 
    if (!get_block_is_free(b3)){ 
        printf("Couldn't free block\n");
        return 0;
    }
    printf("Success!\n");


    if (!compare_next_block(b3, b5) || get_block_capacity(b3) != BLOCK_SIZE + b4_size) {
        printf("Bad capacity!\n");
        return 0;
    }
    
    _free(b5);
    _free(b2);
    _free(b1);
    munmap(heap, get_block_size(b1, THIS));
    return 1;
}

//4
int new_region_extends_old_test() {
    printf("Test 4, no memory left, new memory region extends the old one: \n");

    printf("Trying to initialize heap...    ");
    void* heap = heap_init(MAX_HEAP);
    printf("Success!\n");

    printf("Trying to initialize block 1...   ");
    void* b1 = _malloc(MAX_HEAP);
    printf("Success!\n");

   

    if (heap == NULL){
        printf("Heap corrupted!\n");
        return 0;
    }
    if (b1 == NULL){
        printf("Block 1 corrupted!\n");
        return 0;
    }

    printf("Checking allocation of block 1...   ");
    if (get_block_capacity(b1) != MAX_HEAP || get_next_block(b1) != NULL) {
        printf("Fail\n");
        return 0;
    }
    printf("Success!\n");


    printf("Trying to initialize block 2...   ");
    void* b2 = _malloc(BLOCK_SIZE);
    printf("Success!\n");

    if (b2 == NULL){
        printf("Block 2 corrupted!\n");
        return 0;
    }

    printf("Checking allocation of block 2...   ");
    if (check_max_heap_allocation(b1, b2) || !compare_next_block(b1, b2)) {
        printf("Fail\n");
        return 0;
    }
    printf("Success!\n");

    _free(b1);
    _free(b2);

    munmap(heap, get_block_size(b1, THIS));
    return 1;
}

//5
int new_region_in_new_place() {
    printf("Test 5: no memory left, old memory region can not be extended\n");
    printf("because of other allocated memory regions, new region is allocated\n");
    printf("in a different place: \n");

    printf("Trying to initialize heap...    ");
    void* heap = heap_init(MAX_HEAP);
    printf("Success!\n");

    printf("Trying to initialize block 1...   ");
    void* b1 = _malloc(MAX_HEAP);
    printf("Success!\n");


    if (heap == NULL){
        printf("Heap corrupted!\n");
        return 0;
    }
    if (b1 == NULL){
        printf("Block 1 corrupted!\n");
        return 0;
    }
    if (get_next_block(b1) != NULL) {
        printf("Next block does not exist!\n");
        return 0;
    }
    if(get_block_capacity(b1) != MAX_HEAP){
        printf("Block capacity is wrong!\n");
        return 0;
    }

  
    //#define __USE_MISC 1
    void* p = mmap(calculate_mmap_addr(b1), 
    REGION_MIN_SIZE, 
    PROT_READ | PROT_WRITE,
    MAP_PRIVATE | MAP_FIXED | MAP_ANONYMOUS, 
    -1, 0);

    if (p == MAP_FAILED) {
        printf("MAP_FAILED\n");
        return 0;
    }

    printf("Trying to initialize block 2...   ");
    void* b2 = _malloc(MAX_HEAP);
    printf("Success!\n");

    printf("Checking if block 1 and block 2 are the same block...   ");
    if(compare_blocks(b2, calculate_mmap_addr(b1))){
        printf("Same block!");
        return 0;
    }  
    printf("Success!\n");
    _free(b1);
    munmap(p, REGION_MIN_SIZE);



    
    _free(b2);
    return 1;
}
#endif