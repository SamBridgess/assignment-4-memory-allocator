#ifndef __TESTS_UTIL_H_
#define __TESTS_UTIL_H_

#include "mem.h"
#include "mem_internals.h"


#define HEAP_SIZE 6000
#define BLOCK_SIZE 200
#define MAX_HEAP capacity_from_size((block_size){REGION_MIN_SIZE}).bytes

enum get_param {
    THIS, 
    NEXT
};

size_t get_block_size(void* block, enum get_param param);
size_t get_block_capacity(void* block);
void* calculate_mmap_addr(void* block);
size_t get_block_is_free(void* block);
bool compare_blocks(void* block, void* block2);
bool compare_next_block(void* block, void* block2);
struct block_header* get_next_block(void* block);
bool check_max_heap_allocation(void* block, void* block2);

#endif
