#include "tests.h"
#include <stdio.h>

int main() {
    printf(simple_alloc_test() == 0 ? "Test Fail :(\n\n" : "Test Success!\n\n");
    printf(free_one_block_test() == 0 ? "Test Fail :(\n\n" : "Test Success!\n\n");    
    printf(free_two_blocks_test() == 0 ? "Test Fail :(\n\n" : "Test Success!\n\n");
    printf(new_region_extends_old_test() == 0 ? "Fail :(\n\n" : "Test Success!\n\n");
    printf(new_region_in_new_place() == 0 ? "Fail :(\n\n" : "Test Success!\n\n");

}