#ifndef __TESTS_UTIL_H_
#define __TESTS_UTIL_H_


#include "mem.h"
#include "mem_internals.h"
#include "tests_util.h"
#include <stdio.h>
int simple_alloc_test();            //1
int free_one_block_test();          //2
int free_two_blocks_test();         //3
int new_region_extends_old_test();  //4
int new_region_in_new_place();      //5

#endif