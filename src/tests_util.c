#ifndef _TESTS_UTIL_
#define _TESTS_UTIL_

#include "tests_util.h"


size_t get_block_size(void* block, enum get_param param){
    struct block_header* header = (struct block_header*) ((uint8_t*) block - offsetof(struct block_header, contents));
    block_capacity capacity;
    if(param == THIS) capacity = header->capacity;
    else capacity = header->next->capacity;
    
    block_size bsize = size_from_capacity(capacity);
    return bsize.bytes;
}
size_t get_block_capacity(void* block){
    struct block_header* header = (struct block_header*) ((uint8_t*) block - offsetof(struct block_header, contents));
    block_capacity capacity;
    capacity = header->capacity;
  
    return capacity.bytes;
}
void* calculate_mmap_addr(void* block){
    struct block_header* header = (struct block_header*) ((uint8_t*) block - offsetof(struct block_header, contents));
    void* addr = header->contents + get_block_capacity(block);

    return addr;
}
size_t get_block_is_free(void* block){
    struct block_header* header = (struct block_header*) ((uint8_t*) block - offsetof(struct block_header, contents));
    
    return header->is_free;
}
bool compare_blocks(void* block, void* block2){
    struct block_header* header =  (struct block_header*) ((uint8_t*) block - offsetof(struct block_header, contents));
    
    
    if(header == block2) return true;
    return false;
}
bool compare_next_block(void* block, void* block2){
    struct block_header* header =  (struct block_header*) ((uint8_t*) block - offsetof(struct block_header, contents));
    struct block_header* header1 = header->next;
    struct block_header* header2 = (struct block_header*) ((uint8_t*) block2 - offsetof(struct block_header, contents));
    if(header2 == header1) return true;
    return false;
}
struct block_header* get_next_block(void* block){
    struct block_header* header = (struct block_header*) ((uint8_t*) block - offsetof(struct block_header, contents));
    
    return header->next;
}
bool check_max_heap_allocation(void* block, void* block2){
    struct block_header* header =  (struct block_header*) ((uint8_t*) block - offsetof(struct block_header, contents));
    struct block_header* header2 = (struct block_header*) ((uint8_t*) block2 - offsetof(struct block_header, contents));
    

    if (header != (struct block_header*) (header2->contents + MAX_HEAP))return false;
    return true;
}
#endif